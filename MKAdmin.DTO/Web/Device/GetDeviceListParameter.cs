﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MKAdmin.DTO.Web.Common;

namespace MKAdmin.DTO.Web.Device
{
    public class GetDeviceListParameter : PagingParameter
    {
        /// <summary>
        /// 手机编号
        /// </summary>
        public int MobileId { get; set; }
        /// <summary>
        /// 模块版本号
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 是否Root
        /// </summary>
        public int IsRoot { get; set; }
        /// <summary>
        /// 是否XPosed
        /// </summary>
        public int IsXPosed { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        public string Brand { get; set; }
        /// <summary>
        /// 型号
        /// </summary>
        public string Model { get; set; }
        /// <summary>
        /// 系统OS
        /// </summary>
        public string SystemOS { get; set; }
    }

    public class GetDeviceListModel
    {
        /// <summary>
        /// 手机编号
        /// </summary>
        public int MobileId { get; set; }
        /// <summary>
        /// 1：在线0：离线
        /// </summary>
        public int OnlineStatus { get; set; }

        /// <summary>
        /// 手机保管者
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// IMEI
        /// </summary>
        public string Imei { get; set; }
        /// <summary>
        /// 当前登录微信昵称
        /// </summary>
        public string CurrentWxNickName { get; set; }
        /// <summary>
        /// 当前登录微信头像
        /// </summary>
        public string CurrentWxHeadUrl { get; set; }
        /// <summary>
        /// 当前登录微信号
        /// </summary>
        public string CurrentWxNum { get; set; }
        /// <summary>
        /// 手机电量
        /// </summary>
        public int BatteryLevel { get; set; }
        /// <summary>
        /// 模块版本号
        /// </summary>
        public string ModuleVersion { get; set; }
        /// <summary>
        /// 是否Root
        /// </summary>
        public bool IsRoot { get; set; }
        /// <summary>
        /// 是否XPosed
        /// </summary>
        public bool IsXPosed { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        public string Brand { get; set; }
        /// <summary>
        /// 型号
        /// </summary>
        public string Model { get; set; }
        /// <summary>
        /// 系统OS
        /// </summary>
        public string SystemOS { get; set; }
        /// <summary>
        /// 微信版本
        /// </summary>
        public string WxVersionNo { get; set; }
        /// <summary>
        /// 最后在线时间
        /// </summary>
        public string LastOnlineTime { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string Createtime { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
    /// <summary>
    /// 手机品牌
    /// </summary>
    public class PhoneBrand
    {
        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }
    }
    /// <summary>
    /// 获取手机型号参数
    /// </summary>
    public class GetPhoneModelParameter
    {
        /// <summary>
        /// 手机品牌名称
        /// </summary>
        public string BrandName { get; set; }
    }

    public class PhoneModel
    {
        /// <summary>
        /// 手机型号名称
        /// </summary>
        public string ModelName { get; set; }
    }
    /// <summary>
    /// 获取手机系统参数
    /// </summary>
    public class GetPhoneSystemParameter: GetPhoneModelParameter
    {
        /// <summary>
        /// 手机型号名称
        /// </summary>
        public string ModelName { get; set; }
    }
    public class PhoneSystem
    {
        /// <summary>
        /// 手机系统名称
        /// </summary>
        public string SystemVersion { get; set; }
    }
    /// <summary>
    /// 更新备注信息
    /// </summary>
    public class UpdateRemarkParameter
    {
        /// <summary>
        /// 手机Id
        /// </summary>
        public int MobileId { get; set; }
        /// <summary>
        /// 备注信息
        /// </summary>
        public string Remark { get; set; }
    }
    /// <summary>
    /// 获取设备保管者参数
    /// </summary>
    public class GetDeviceCurrentKeeperParameter
    {
        public int MobileId { get; set; }
    }
    /// <summary>
    /// 获取当前设备保管者
    /// </summary>
    public class GetDeviceCurrentKeeper
    {
        public string CurrentKeeper { get; set; }
    }

    public class KeeperSettingParameter : GetDeviceCurrentKeeperParameter
    {
        public int NewAgentId { get; set; }
    }
    /// <summary>
    /// 删除设备参数
    /// </summary>
    public class DeleteDeviceParameter: GetDeviceCurrentKeeperParameter
    {

    }
    /// <summary>
    /// 获取登录手机微信历史记录参数
    /// </summary>
    public class PhoneWxhistoryListParameter : PagingParameter
    {
        public int MobileId { get; set; }
    }
    /// <summary>
    /// 获取登录手机微信历史记录
    /// </summary>
    public class GetPhoneWxHistoryListModel
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 微信Id
        /// </summary>
        public string WxId { get; set; }

        /// <summary>
        /// 微信昵称
        /// </summary>
        public string WxNickName { get; set; }

        /// <summary>
        /// 微信头像Url
        /// </summary>
        public string WxHeadUrl { get; set; }

        /// <summary>
        /// 微信性别
        /// </summary>
        public int WxSex { get; set; }

        /// <summary>
        /// 微信号
        /// </summary>
        public string WxNum { get; set; }
        /// <summary>
        /// 微信版本
        /// </summary>
        public string WxVersion { get; set; }
        /// <summary>
        /// 绑定时间
        /// </summary>
        public string BindedTime { get; set; }
    }
}
