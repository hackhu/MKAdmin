﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Table.BasicTable
{
    public class DelBasicTableParameter
    {
        /// <summary>
        /// 员工编号
        /// </summary>
        public int employeeId { get; set; }
    }
}
