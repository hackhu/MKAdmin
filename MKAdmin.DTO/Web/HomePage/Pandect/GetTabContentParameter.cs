﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.HomePage.Pandect
{
    public class GetTabContentParameter
    {
        public int TabId { get; set; }
    }
}
