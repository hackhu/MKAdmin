﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.HomePage.Pandect
{
    public class CollectListModel
    {
        /// <summary>
        /// 好友总数
        /// </summary>
        public int friendsSumCount { get; set; }
        /// <summary>
        /// 好友男总数
        /// </summary>
        public int friendsMaleCount { get; set; }
        /// <summary>
        /// 好友女总数
        /// </summary>
        public int friendsFemaleCount { get; set; }
        /// <summary>
        /// 好友未知总数
        /// </summary>
        public int friendsUnknownCount { get; set; }
        /// <summary>
        /// 新增好友总数
        /// </summary>
        public int newFriendsSumCount { get; set; }
        /// <summary>
        /// 新增好友男总数
        /// </summary>
        public int newFriendsMaleCount { get; set; }
        /// <summary>
        /// 新增好友女总数
        /// </summary>
        public int newFriendsFemaleCount { get; set; }
        /// <summary>
        /// 新增好友未知总数
        /// </summary>
        public int newFriendsUnknownCount { get; set; }
        /// <summary>
        /// 群总数
        /// </summary>
        public int groupSumCount { get; set; }
        /// <summary>
        /// 成为群主数
        /// </summary>
        public int groupOwnerCount { get; set; }
        /// <summary>
        /// 群成员数
        /// </summary>
        public int groupGroupMemberCount { get; set; }
        /// <summary>
        /// 微信总数
        /// </summary>
        public int wxSumCount { get; set; }
        /// <summary>
        /// 扫码注册手机数
        /// </summary>
        public int wxQRCodeCount { get; set; }
        /// <summary>
        /// 已登录微信数
        /// </summary>
        public int wxLoginedWxCount { get; set; }
    }
}
