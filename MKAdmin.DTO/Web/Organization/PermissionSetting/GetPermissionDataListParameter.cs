﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Organization.PermissionSetting
{
    public class GetPermissionDataListParameter
    {
        /// <summary>
        /// 权限ID
        /// </summary>
        public int rightId { get; set; }
    }
}
