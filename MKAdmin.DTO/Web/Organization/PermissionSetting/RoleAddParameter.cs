﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Organization.PermissionSetting
{
    public class RoleAddParameter
    {
        /// <summary>
        /// 权限名称
        /// </summary>
        public string RightName { get; set; }
        /// <summary>
        /// 权限备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 选择的权限节点
        /// </summary>
        public List<int> RightIdsList { get; set; }
    }
    /// <summary>
    /// 节点结构
    /// </summary>
    public class PermissionDTO
    {
        /// <summary>
        /// 父ID
        /// </summary>
        public string PermissionId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string PermissionName { get; set; }
        /// <summary>
        /// ID
        /// </summary>
        public string FPermissionId { get; set; }
        /*/// <summary>
        /// 
        /// </summary>
        public string State { get; set; }*/
    }

    public class RightInfo
    {
        public int RightId { get; set; }

        public int RightParentId { get; set; }
        public bool IsLog { get; set; }
        public int TypeCode { get; set; }
        public int StatusCode { get; set; }
        public int OrderNo { get; set; }
        public string RightName { get; set; }
        public string IconName { get; set; }
        public string ResourceName { get; set; }
        public string MenuPageUrl { get; set; }
        public string Remark { get; set; }
    }
    public class TreeNode
    {
        public string ParentID { get; set; }

        public string ID { get; set; }

        public string Name { get; set; }

        public string State { get; set; }

        public bool? Checked { get; set; }

        public string IconCls { get; set; }

        public string Url { get; set; }
    }
}
