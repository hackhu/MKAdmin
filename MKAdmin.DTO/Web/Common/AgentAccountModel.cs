﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Common
{
    [Serializable]
    public class AgentAccountModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int AgentId { get; set; }

        /// <summary>
        /// 登录账号名(admin)
        /// </summary>
        public string AgentName { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 1：上级账号  2：下级账号 3：客服账号  10：运营账号开设的账号
        /// </summary>
        public int TypeCode { get; set; }

        /// <summary>
        /// 上级Id
        /// </summary>
        public int ParentId { get; set; }
    }
}
