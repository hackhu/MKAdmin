﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Common
{
    public class PagingList<T>
    {
        public PagingList()
        {
            data = new List<T>();
        }

        public PagingList(IEnumerable<T> allItems, int _pageIndex, int _pageSize)
        {
            //pageIndex = _pageIndex;
            //pageSize = _pageSize;
            var items = allItems as IList<T> ?? allItems.ToList();
            count = items.Count;
            data = items.Skip((_pageIndex - 1) * _pageSize).Take(_pageSize).ToList();
        }

        public PagingList(IEnumerable<T> currentPageItems, int _pageIndex, int _pageSize, int totalItemCount)
        {
            //pageIndex = _pageIndex;
            //pageSize = _pageSize;
            count = totalItemCount;
            data = currentPageItems.ToList();
        }

        ///// <summary>
        ///// 页索引
        ///// </summary>
        //public int pageIndex { get; set; }
        ///// <summary>
        ///// 每页记录数
        ///// </summary>
        //public int pageSize { get; set; }

        ///// <summary>
        ///// 
        ///// </summary>
        public int code = 0;
        ///// <summary>
        ///// 
        ///// </summary>
        public string msg = "";
        /// <summary>
        /// 总记录数
        /// </summary>
        public int count { get; set; }

        /// <summary>
        /// 总页数
        /// </summary>
        //public int pageCount => pageSize > 0 ? (count / pageSize + (count % pageSize == 0 ? 0 : 1)) : 0;

        /// <summary>
        /// 数据集合
        /// </summary>
        public List<T> data { get; set; }
    }
}
