﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Common
{
    [Serializable]
    public class UserInfoModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int OperatorId { get; set; }

        /// <summary>
        /// 登录账号名(admin)
        /// </summary>
        public string OperatorNo { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string OperatorName { get; set; }
        /// <summary>
        /// 主账号
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 0：启用  1：停用  2：删除
        /// </summary>
        public int AccountStatusCode { get; set; }

        /// <summary>
        /// 到期时间
        /// </summary>
        public DateTime ExpireTime { get; set; }
    }
}
