﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MKAdmin.DTO.Web.Common;
using MKAdmin.DTO.Web.Login;

namespace MKAdmin.IService.Web.Login
{
    public interface ILoginService
    {
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        Result<UserInfoModel> UserLogin(LoginInfoParameter parameter);

        /// <summary>
        /// 用户登录日志
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        Result UserLoginLog(string operatorNo, bool loginStatus);

        /// <summary>
        /// 获取用户账号和下级账号信息('admin','mike','stev')
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        Result<List<AgentAccountModel>> GetMainAndSubAccountList(GetSubAccountParameter parameter);

        /// <summary>
        /// 验证用户状态
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        Result<UserInfoModel> VerifyUserStatus(UserInfoModel userInfo);
    }
}
