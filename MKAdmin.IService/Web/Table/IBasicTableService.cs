﻿using MKAdmin.DTO.Web.Common;
using MKAdmin.DTO.Web.Table.BasicTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.IService.Web.Table
{
    public interface IBasicTableService
    {
        /// <summary>
        /// 获取员工信息列表
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        PagingList<GetBasicTableListModel> List(GetBasicTableListParameter parameter);

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        Result Add(AddBasicTableParameter parameter, UserInfoModel userInfo);

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        Result Edit(EditBasicTableParameter parameter);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        Result Del(DelBasicTableParameter parameter);

    }
}
