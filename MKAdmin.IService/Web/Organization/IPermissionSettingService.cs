﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MKAdmin.DTO.Web.Common;
using MKAdmin.DTO.Web.Organization.PermissionSetting;

namespace MKAdmin.IService.Web.Organization
{
    public interface IPermissionSettingService
    {
        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        PagingList<GetPermissionListModel> List(GetPermissionListParameter parameter, UserInfoModel info);

        PagingList<GetPermissionListModel> AllotList(GetPermissionAllotListParameter parameter, UserInfoModel userInfo);

        /// <summary>
        /// 权限新增
        /// </summary>
        /// <returns></returns>
        Result AddRole(RoleAddParameter parameter, UserInfoModel info);
        /// <summary>
        /// 权限删除
        /// </summary>
        /// <returns></returns>
        Result DeleteRole(RoleDelParameter parameter, UserInfoModel info);

        /// <summary>
        /// 权限编辑
        /// </summary>
        /// <returns></returns>
        Result EditRole(RoleEditSaveParameter parameter, UserInfoModel info);
        /// <summary>
        /// 获取权限功能信息表
        /// </summary>
        /// <returns></returns>
        Result<List<PermissionDTO>> GetPermissionInfo();
        /// <summary>
        /// 根据Id获取自身权限
        /// </summary>
        /// <param name="agentId"></param>
        /// <returns></returns>
        List<RightInfo> GetPermissionInfoById(int agentId);
        /// <summary>
        /// 根据权限ID获取角色权限
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        Result<GetRoleRightModel> GetRoleRightByRoleId(RoleEditParameter parameter);

        /// <summary>
        /// 获取用户菜单权限信息
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        Result<GetPermissionDataListModel> PermissionMenuList(UserInfoModel userInfo);

        /// <summary>
        /// 获取用户功能权限信息
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        Result<GetPermissionDataListModel> PermissionFunctionList(int rightId, UserInfoModel userInfo);

        /// <summary>
        /// 验证当前用户是否拥有该资源名称权限
        /// </summary>
        /// <param name="resourceName"></param>
        /// <returns></returns>
        Result<bool> UserPermissionVerify(string resourceName, UserInfoModel userInfo);

    }
}
