﻿using MKAdmin.Web.App_Start;
using MKAdmin.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MKAdmin.Web.Areas.Form.Controllers
{
    /// <summary>
    /// CheckBox复选框
    /// </summary>
    public class CheckBoxController : BaseMvcController
    {
        // GET: Form/CheckBox
        public ActionResult Index()
        {
            return View(PageViewFilesConfig.FormCheckBoxIndex);
        }
    }
}