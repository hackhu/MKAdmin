﻿using MKAdmin.Web.App_Start;
using MKAdmin.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MKAdmin.Web.Areas.Form.Controllers
{
    /// <summary>
    /// 下拉框
    /// </summary>
    public class DropDownController : BaseMvcController
    {
        // GET: Form/DropDown
        public ActionResult Index()
        {
            return View(PageViewFilesConfig.FormDropDownIndex);
        }
    }
}