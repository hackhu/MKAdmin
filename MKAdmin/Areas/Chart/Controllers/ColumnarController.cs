﻿using MKAdmin.Web.App_Start;
using MKAdmin.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MKAdmin.Web.Areas.Chart.Controllers
{
    /// <summary>
    /// 柱状图
    /// </summary>
    public class ColumnarController : BaseMvcController
    {
        // GET: Chart/Columnar
        public ActionResult Index()
        {
            return View(PageViewFilesConfig.ChartColumnarIndex);
        }
    }
}