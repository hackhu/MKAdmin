﻿using MKAdmin.Web.App_Start;
using MKAdmin.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MKAdmin.Web.Areas.File.Controllers
{
    public class UploadController : BaseMvcController
    {
        // GET: File/Upload
        public ActionResult Index()
        {
            return View(PageViewFilesConfig.FileUploadIndex);
        }
    }
}