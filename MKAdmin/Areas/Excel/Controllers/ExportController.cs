﻿using MKAdmin.DTO.Web.Common;
using MKAdmin.DTO.Web.Table.BasicTable;
using MKAdmin.IService.Web.Table;
using MKAdmin.ToolKit;
using MKAdmin.ToolKit.FileKit;
using MKAdmin.Web.App_Start;
using MKAdmin.Web.Controllers;
using Ninject;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MKAdmin.Web.Areas.Excel.Controllers
{
    public class ExportController : BaseMvcController
    {
        [Inject]
        public IBasicTableService BasicTableService { set; get; }

        // GET: Excel/Export
        public ActionResult Index()
        {
            return View(PageViewFilesConfig.ExcelExportIndex);
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Export(GetBasicTableListParameter parameter)
        {
            var exportResult = new Result<UploadFileModel>() { data = new UploadFileModel() };

            parameter.pageIndex = 1;
            parameter.pageSize = 9999999;
            var result = BasicTableService.List(parameter);

            if (result != null && result.data != null && result.data.Count > 0)
            {
                DataTable dtlist = new DataTable();
                dtlist.Columns.Add("EmployeeId", typeof(int));
                dtlist.Columns.Add("EmployeeName", typeof(string));
                dtlist.Columns.Add("EmployeeAge", typeof(int));
                dtlist.Columns.Add("Education", typeof(string));
                dtlist.Columns.Add("ContactPhone", typeof(string));

                foreach (var item in result.data)
                {
                    dtlist.Rows.Add(item.EmployeeId, item.EmployeeName, item.EmployeeAge
                        , item.Education, item.ContactPhone);
                }
                var catalogConfig = FileCatalogHelper.Catalog["Excel"];

                exportResult.data.catalog = $@"\{ DateTime.Now.ToString("yyyyMM")}\{ DateTime.Now.ToString("yyyyMMdd")}";
                exportResult.data.fileName = $@"{ Guid.NewGuid().ToString("N")}.xlsx";

                //保存到临时目录下
                string fileCatalog = $@"{ catalogConfig.TempFile.AbsolutePath }{exportResult.data.catalog}";
                if (!Directory.Exists(fileCatalog))
                    Directory.CreateDirectory(fileCatalog);

                string filePath = $@"{ fileCatalog}\{exportResult.data.fileName}";

                Dictionary<string, string> headerName = new Dictionary<string, string>();
                headerName.Add("EmployeeId", "编号");
                headerName.Add("EmployeeName", "姓名");
                headerName.Add("EmployeeAge", "年龄");
                headerName.Add("Education", "学历");
                headerName.Add("ContactPhone", "联系方式");

                DataTable dtSource = dtlist.ReplaceHeader(headerName);

                //设置Excel列宽,非必须
                Dictionary<string, double> headerWidth = new Dictionary<string, double>();
                headerWidth.Add("编号", 10);
                headerWidth.Add("姓名", 10);
                headerWidth.Add("年龄", 8);
                headerWidth.Add("学历", 10);
                headerWidth.Add("联系方式", 20);

                ExcelHelper.Save(dtSource, filePath, headerWidth);
            }

            return Json(exportResult, JsonRequestBehavior.DenyGet);
        }

    }
}