﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MKAdmin.DTO.Web.Common;
using MKAdmin.ToolKit;

namespace MKAdmin.Web.Authorizes
{
    public static class CurrentUser
    {
        public static int AgentId
        {
            get
            {
                return GetLoginInfo().OperatorId;
            }
        }

        /// <summary>
        /// 当前登录的AgentName
        /// </summary>
        public static string AgentName
        {
            get
            {
                return GetLoginInfo().OperatorNo;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static UserInfoModel GetLoginInfo()
        {
            UserInfoModel info = HttpContext.Current.Session[AuthConfig.SESSION_LOGIN_KEY] as UserInfoModel;
            if (info == null || info.OperatorId <= 0)
            {
                info = Loginer.AnalysisTicketData();
                if (info == null)
                {
                    Loginer.SignOut();
                }

                HttpContext.Current.Session[AuthConfig.SESSION_LOGIN_KEY] = info;
            }
            return info;
        }
    }
}