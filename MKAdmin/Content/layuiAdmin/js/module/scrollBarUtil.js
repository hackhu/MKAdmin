﻿layui.define(['jquery', 'layer'], function (exports) {
    var $ = layui.jquery;
    var layer = layui.layer;
    var widW = $(window).width(); //窗口宽度
    var widH = $(window).height();//窗口高度
    /**/
    var scrollBarUtil = {
        setScrollBar: function (doc, ht) {
                $(doc).css({ 'height': widH - ht + 'px', 'overflow-y': 'scroll', 'overflow-x': 'visible' })
                $(window).resize(function () {
                    var widH = $(window).innerHeight();
                    $(doc).css({ 'height': widH - ht + 'px' })
                })
        }
    }
    //输出test接口
    exports('scrollBarUtil', scrollBarUtil);
});
