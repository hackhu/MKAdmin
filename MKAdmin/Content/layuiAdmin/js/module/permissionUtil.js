﻿
layui.define(['jquery', 'ajaxUtil'], function (exports) {
    var $ = layui.jquery,
        ajaxutil = layui.ajaxUtil;

    var permissionUtil = {
        setting: function (id, succFun) {
            ajaxutil.request({
                url: '/organization/permissionsetting/permissionfunctionlist',
                async: true,
                data: {
                    rightId: id
                },
                success: function (res) {
                    //功能权限
                    var functionData = res.data.functionData;
                    if (functionData != null && functionData.length > 0) {
                        $.each(functionData, function (index, item) {
                            var nodeName = ".permission-right-" + item;
                            $(nodeName).removeClass("perssion-node-display");
                        });
                    }
                    if (typeof (succFun) == 'function') {
                        return succFun(res);
                    }
                }
            });
        }
    };

    //输出test接口
    exports('permissionUtil', permissionUtil);
})
