﻿layui.define(['jquery', 'layer'], function (exports) {
    var $ = layui.jquery, layer = layui.layer;

    var ajaxUtil = {
        /**/
        request: function (jsonData) {
            var parameter = {
                url: '',
                type: 'POST',
                async: true,
                isLoading: true,
                data: {},
                success: function () {

                },
                error: function () {

                }
            }

            $.extend(parameter, jsonData);

            if (parameter.isLoading == true) {
                if (parameter.type.toLowerCase() == 'get') {
                    layer.load(2, {
                        shade: [0.3, '#fff']     //0.1透明度的白色背景
                    });
                }
                else {
                    layer.load(0, {
                        shade: [0.3, '#fff']     //0.1透明度的白色背景
                    });
                }
            }

            $.ajax({
                type: parameter.type,
                data: parameter.data,
                url: parameter.url,//发送请求
                async: parameter.async,
                dataType: 'json',
                success: function (result) {
                    if (result !== null) {
                        var msg = result.msg;
                        if (msg == "100000") {
                            window.location.href = '/login';
                            return;
                        }
                        else if (msg == "100001") {
                            layer.msg('您没有操作权限');
                            return;
                        }
                    }
                    if (typeof (parameter.success) == 'function') {
                        return parameter.success(result);
                    }
                },
                complete: function (res) {
                    if (parameter.isLoading == true) {
                        layer.closeAll('loading');
                    }
                },
                error: function (res) {
                    layer.msg('操作异常,请稍后重试');
                }
            })
        }
    };
    //输出test接口
    exports('ajaxUtil', ajaxUtil);
});
