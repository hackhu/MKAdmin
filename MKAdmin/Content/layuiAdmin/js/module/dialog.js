layui.define(['layer'], function (exports) {
    var layer = layui.layer;
    var dialog = {
        //提示语信息
        alert: {
            /*确认框*/
            confirm: function (jsonData) {
                layer.confirm(jsonData.message, {
                    btn: ['确定', '取消'],
                    shade: [0.1, '#fff']
                }, function (index) {
                    jsonData.success(index);
                });
            }
            , success: function (msg) {
                //icon
                //1:成功 2:加载中  3:疑问   4:安全锁   5:失败   6:微笑  其他:警告
                layer.msg(msg, {
                    icon: 1
                    , shade: 0.01
                    , time: 1500
                });
            }
            , warn: function (msg) {
                layer.msg(msg, {
                    icon: 16
                    , shade: 0.01
                    , time: 1500
                });
            }
            , fail: function (msg) {
                layer.msg(msg, {
                    icon: 5
                    , shade: 0.01
                    , time: 1500
                });
            }
            , smile: function (msg) {
                layer.msg(msg, {
                    icon: 6
                    , shade: 0.01
                    , time: 1500
                });
            }
            , msg: function (msg) {
                layer.msg(msg, { time: 1500 });
            }
            , postLoading: function () {
                layer.load(0, {
                    shade: [0.3, '#fff']     //0.1透明度的白色背景
                });
            }
            , getLoading: function () {
                layer.load(2, {
                    shade: [0.3, '#fff']     //0.1透明度的白色背景
                });
            }
            /**
            * 提示
            * @param title
            * @param obj
            */
            , tips: function (title, obj) {
                layer.tips(title, obj, {
                    tips: [1, '#444c63'],   //还可配置颜色
                    time: 1000
                });
            }
        }
        , close: function (index) {
            layer.close(index);
        }
        , closeAll: function (type) {
            //dialog: 关闭信息框
            //dialog: 关闭所有页面层
            //dialog: 关闭所有的iframe层
            //dialog: 关闭加载层
            //dialog: 关闭所有的tips层
            if (type == null || type == undefined || type == "") {
                layer.closeAll();
            }
            else {
                layer.closeAll(type);
            }
        }
        , page: function (title, url, w, h) {
            if (title == null || title == '') {
                title = false;
            }
            ;
            if (url == null || url == '') {
                url = "404.html";
            }
            ;
            if (w == null || w == '') {
                w = '700px';
            }
            ;
            if (h == null || h == '') {
                h = '350px';
            }
            ;
            var index = layer.open({
                type: 2,
                title: title,
                area: [w, h],
                fixed: false, //不固定
                maxmin: true,
                content: url
            });
        },
        open: function (settings) {
            var param = {
                dialogId: '',
                title: '标题',
                width: 360,
                height: 300,
                ok: null,
                cancel: null
            };
            $.extend(param, settings);

            var dialogId = 'layerDialog_' + (new Date()).valueOf();
            layer.open({
                type: 1
                , offset: 'auto'
                , title: param.title
                // skin: 'layui-layer-rim' //加上边框
                , id: dialogId
                , closeBtn: 1
                , content: $('#' + param.dialogId)
                , btn: param.ok != null ? ['确定', '取消'] : null
                , area: [param.width + 'px', param.height + 'px']  //宽、高自适应
                , btnAlign: 'r' //按钮位置
                , shade: 0.3 //设置遮罩
                , yes: function (index) {
                    if (typeof (param.ok) == 'function') {
                        param.ok(index);
                    }
                }
                , btn2: function () {
                    if (typeof (param.cancel) == 'function') {
                        param.cancel();
                    }
                }
                , cancel: function () {
                }
            });

            //解决下拉框选择被遮挡问题
            //存在select下拉框才执行
            var layuiFormSelect = $("#" + dialogId + " select");
            if (layuiFormSelect != null && layuiFormSelect.length > 0) {
                $("#" + dialogId).css('overflow', 'visible');
            }
        }
        , openUrl: function (settings) {
            var param = {
                url: '',
                title: '标题',
                width: 360,
                height: 300
            };
            $.extend(param, settings);

            var dialogId = 'layerDialog_' + (new Date()).valueOf();
            layer.open({
                type: 2
                , offset: 'auto'
                , title: param.title
                , id: dialogId
                //, closeBtn: 1
                , content: [param.url]
                , area: [param.width + 'px', param.height + 'px']  //宽、高自适应
                , shade: 0.3 //设置遮罩
            });
        }
    };
    //输出test接口
    exports('dialog', dialog);
});
