﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MKAdmin.Web.Startup))]
namespace MKAdmin.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //ConfigureAuth(app);
            app.MapSignalR();    //主要是这句
        }
    }
}