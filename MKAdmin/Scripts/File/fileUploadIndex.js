﻿//上传文件
layui.use(['jquery', 'uploadUtil'], function () {
    var $ = layui.jquery,
        uploadUtil = layui.uploadUtil;

    //上传控件初始化
    uploadUtil.render({
        elemId: 'btn_FileUpload'
        //填写需要上传的文件后缀名
        , exts: 'xls|xlsx|doc|docx|jpg|jpeg|png|bmp|gif|zip|rar|txt|ppt|pptx|pdf'
        , data: {
            uploadfile_ant: 'Upload'
        }
        , done: function (res) {
            console.log(res);
            var uploadJson = [];
            uploadJson.push(res.data);
            generateEle(uploadJson);
        }
    });

    //生成元素
    var generateEle = function (uploadJson) {
        $.each(uploadJson, function (index, item) {
            var imgUrl = "";
            var catalog = item.catalog;
            var fileName = item.fileName;

            var suffixName = getFileSuffix(fileName);
            switch (suffixName) {
                case "jpg":
                case "jpeg":
                case "png":
                case "bmp":
                case "gif":
                    imgUrl = item.url;
                    break;
                case "zip":
                case "rar":
                    imgUrl = "/images/rar.png";
                    break;
                case "txt":
                    imgUrl = "/images/txt.png";
                    break;
                case "ppt":
                case "pptx":
                    imgUrl = "/images/ppt.png";
                    break;
                case "pdf":
                    imgUrl = "/images/pdf.png";
                    break;
                case "xls":
                case "xlsx":
                    imgUrl = "/images/excel.png";
                    break;
                case "doc":
                case "docx":
                    imgUrl = "/images/word.png";
                    break;
                default:
                    break;
            }

            var fileHtml =
                ' <div style = "margin-left:20px;margin-top:10px;float:left" >' +
                '     <div>' +
                '         <img src="' + imgUrl + '" style="width:128px;height:128px;" />' +
                '     </div>' +
                '     <div style="padding-left: 10px;margin-top: 2px;">' +
                '         <a href="/home/downloadconfigfile?key=Upload&catalog=' + catalog + '&filename=' + fileName + '&type=0"' +
                '               class="layui-btn layui-btn-normal layui-btn-sm btn_color_confirm cls-file-upload-download">下载</a>' +
                '         <a class="layui-btn layui-btn-normal layui-btn-sm btn_color_confirm cls-file-upload-del">删除</a>' +
                '     </div>' +
                ' </div > ';

            $(".cls-file-upload-index-show").prepend(fileHtml);
        });

        createClickEvent();
    }

    //创建点击事件
    var createClickEvent = function () {
        //删除
        $(".cls-file-upload-del").unbind('click');
        $(".cls-file-upload-del").click(function () {
            $(this).parent().parent().remove();
        });
    }
    var getFileSuffix = function (fileName) {
        var suffixName = '';
        var fileNameSplit = fileName.split('.');
        if (fileNameSplit.length > 0) {
            suffixName = fileNameSplit[fileNameSplit.length - 1];
        }

        return suffixName;
    }

    //设置默认展示文件
    var jsonFileDefault = [{
        catalog: '/201901/20190130',
        fileName: 'cropDefault.jpg',
        url: '/UploadFiles/Upload/temp/201901/20190130/cropDefault.jpg'
    }, {
            catalog: '/201901/20190130',
            fileName: 'txttest.txt',
            url: '/UploadFiles/Upload/temp/201901/20190130/txttest.txt'
        }];

    generateEle(jsonFileDefault);
});
