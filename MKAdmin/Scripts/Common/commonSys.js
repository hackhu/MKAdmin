﻿layui.use(['jquery'], function () {
    var $ = layui.jquery;

    $('.zf_Sign_out').hover(function () {
        $('.zf_Sign_out_list').show()
        $(this).find('i').css({
            'transform': 'rotate(-180deg)',
            'margin-top': '42px'
        })
    }, function () {
        $('.zf_Sign_out_list').hide()
        $(this).find('i').css({
            'transform': 'rotate(0deg)',
            'margin-top': '0px'
        })

    })
})
