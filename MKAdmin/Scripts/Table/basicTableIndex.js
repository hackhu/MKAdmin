﻿//表格管理 - 基本表格
layui.use(['form', 'jquery', 'laydate', 'dialog', 'table', 'laypage', 'ajaxUtil', 'permissionUtil', 'switchTab'], function () {
    var table = layui.table,
        laypage = layui.laypage,
        $ = layui.jquery,
        dialog = layui.dialog,
        ajaxutil = layui.ajaxUtil,
        laydate = layui.laydate,
        form = layui.form,
        permissionUtil = layui.permissionUtil,
        switchTab = layui.switchTab;
    //permissionUtil.setting(100000);

    //表格渲染
    table.render({
        id: 'cn_GridBasicTable'
        , elem: '#grid_BasicTable'
        , method: 'post'
        , url: '/table/basictable/list'
        //启动分页
        , page: true
        , height: 'full-100'
        , loading: true
        , text: { none: '未查询到数据' }
        , limit: 20
        , limits: [20, 50, 100]
        //禁用前端自动排序
        , autoSort: false
        , where: {
            name: ''
        }
        , request: {
            pageName: 'pageIndex'
            , limitName: 'pageSize'
        }
        , cols: [[
            { field: 'EmployeeId', width: 80, title: '编号' }
            , { field: 'EmployeeName', width: 150, title: '姓名' }
            , { field: 'EmployeeAge', title: '年龄', width: 100, sort: true }
            , { field: 'Education', width: 180, title: '学历' }
            , { field: 'ContactPhone', width: 150, title: '联系方式' }
            , { fixed: 'right', title: '操作', toolbar: '#barOperation', width: 150 }
        ]]
    });

    //监听排序事件 
    table.on('sort(ft_BasicTable)', function (obj) {
        if (obj.type !== null) {
            table.reload('cn_GridBasicTable', {
                initSort: obj
                , where: {
                    sortName: obj.field //排序字段
                    , sortOrder: obj.type //排序方式
                    , name: $('#txt_BasicIndex_Name').val()
                }
            });
        }
    });

    //监听行工具事件
    table.on('tool(ft_BasicTable)', function (obj) {
        var rowData = obj.data;
        if (obj.event === 'del') {
            var employeeId = rowData.EmployeeId;
            dialog.alert.confirm({
                message: '确定删除该用户吗？'
                , success: function (index) {
                    dialog.close(index);
                    ajaxutil.request({
                        url: '/table/basictable/del',
                        data: {
                            employeeId: employeeId
                        },
                        success: function (res) {
                            if (res.status) {
                                dialog.alert.msg('删除成功');

                                var pageIndex = $(".layui-laypage-skip").find("input").val();
                                //刷新页面
                                loadData({
                                    name: $('#txt_BasicIndex_Name').val()
                                }, pageIndex)
                            } else {
                                dialog.alert.fail(res.msg);
                            }
                        }
                    });
                }
            });
        } else if (obj.event === 'edit') {
            initEditForm(rowData);
            dialog.open({
                dialogId: 'dialog_BasicTable_Add'
                , width: 600
                , height: 330
                , title: '修改'
                , ok: function (index) {
                    editEmployee(index, rowData.EmployeeId);
                }
                , cancel: function () {

                }
            });
        }
    });

    var loadData = function (param, pageIndex) {
        //执行重载
        table.reload('cn_GridBasicTable', {
            page: {
                curr: pageIndex //刷新指定页
            }
            , where: param
        });
    }

    //筛选
    $('#btn_BasicTable_Search').on('click', function () {
        loadData({
            name: $('#txt_BasicIndex_Name').val()
        }, 1)
    });

    //添加
    $('#btn_BasicTable_Add').on('click', function () {
        dialog.open({
            dialogId: 'dialog_BasicTable_Add'
            , width: 600
            , height: 330
            , title: '添加'
            , ok: function (index) {
                addEmployee(index);
            }
            , cancel: function () {

            }
        });
    });

    var addEmployee = function (index) {
        var employeeName = $(".cs_BasicTableEmployeeName").val();
        var employeeAge = $(".cs_BasicTableEmployeeAge").val();
        var employeeSex = $(".cs_BasicTableEmployeeSex").val();
        var employeeHeight = $(".cs_BasicTableEmployeeHeight").val();
        var employeeEducation = $(".cs_BasicTableEmployeeEducation").val();
        var employeePhone = $(".cs_BasicTableEmployeePhone").val();

        ajaxutil.request({
            url: '/table/basictable/add',
            data: {
                employeeName: employeeName
                , employeeAge: employeeAge
                , employeeSex: employeeSex
                , employeeHeight: employeeHeight
                , employeeEducation: employeeEducation
                , employeePhone: employeePhone
            },
            success: function (res) {
                if (res.status) {
                    dialog.close(index);
                    dialog.alert.success('添加成功');

                    $('#txt_BasicIndex_Name').val('');
                    //刷新页面
                    loadData({
                        name: ''
                    }, 1)

                    clearForm();
                } else {
                    dialog.alert.fail(res.msg);
                }
            }
        });
    }

    var editEmployee = function (index, employeeId) {
        var employeeName = $(".cs_BasicTableEmployeeName").val();
        var employeeAge = $(".cs_BasicTableEmployeeAge").val();
        var employeeSex = $(".cs_BasicTableEmployeeSex").val();
        var employeeHeight = $(".cs_BasicTableEmployeeHeight").val();
        var employeeEducation = $(".cs_BasicTableEmployeeEducation").val();
        var employeePhone = $(".cs_BasicTableEmployeePhone").val();

        ajaxutil.request({
            url: '/table/basictable/edit',
            data: {
                employeeId: employeeId
                , employeeName: employeeName
                , employeeAge: employeeAge
                , employeeSex: employeeSex
                , employeeHeight: employeeHeight
                , employeeEducation: employeeEducation
                , employeePhone: employeePhone
            },
            success: function (res) {
                if (res.status) {
                    dialog.close(index);
                    dialog.alert.success('修改成功');

                    //当前页码（如果当前页存在多个表格，则需要换种写法）
                    var pageIndex = $(".layui-laypage-skip").find("input").val();

                    //刷新指定页面
                    loadData({
                        name: $('#txt_BasicIndex_Name').val()
                    }, pageIndex);

                    clearForm();
                } else {
                    dialog.alert.fail(res.msg);
                }
            }
        });
    }

    var initEditForm = function (rowData) {
        $(".cs_BasicTableEmployeeName").val(rowData.EmployeeName);
        $(".cs_BasicTableEmployeeAge").val(rowData.EmployeeAge);
        $(".cs_BasicTableEmployeeSex").val(rowData.EmployeeSex);
        $(".cs_BasicTableEmployeeHeight").val(rowData.EmployeeHeight);
        $(".cs_BasicTableEmployeeEducation").val(rowData.Education);
        $(".cs_BasicTableEmployeePhone").val(rowData.ContactPhone);

        //更新 lay-filter="ft_BasicTableForm" 所在容器内的全部表单状态
        form.render(null, 'ft_BasicTableForm');
    }

    ///清空表单
    var clearForm = function () {
        $(".cs_BasicTableEmployeeName").val('');
        $(".cs_BasicTableEmployeeAge").val('');
        $(".cs_BasicTableEmployeeSex").val('');
        $(".cs_BasicTableEmployeeHeight").val('');
        $(".cs_BasicTableEmployeeEducation").val('');
        $(".cs_BasicTableEmployeePhone").val('');

        //更新 lay-filter="ft_BasicTableForm" 所在容器内的全部表单状态
        form.render(null, 'ft_BasicTableForm');
    }
});