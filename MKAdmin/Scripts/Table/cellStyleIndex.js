﻿//表格管理 - 基本表格
layui.use(['form', 'jquery', 'laydate', 'layer', 'dialog', 'table', 'laypage'
    , 'ajaxUtil', 'permissionUtil'], function () {
        var table = layui.table,
            laypage = layui.laypage,
            $ = layui.jquery,
            layer = layui.layer,
            dialog = layui.dialog,
            ajaxutil = layui.ajaxUtil,
            laydate = layui.laydate,
            form = layui.form,
            permissionUtil = layui.permissionUtil;

        //permissionUtil.setting(100000);

        //表格渲染
        table.render({
            id: 'cn_GridCellStyleTable'
            , elem: '#grid_CellStyleTable'
            , method: 'post'
            , url: '/table/basictable/list'
            //启动分页
            , page: true
            , height: 'full-100'
            , loading: true
            , text: { none: '未查询到数据' }
            , limit: 20
            , limits: [20, 50, 100]
            //禁用前端自动排序
            , autoSort: false
            , where: {
                name: ''
            }
            , request: {
                pageName: 'pageIndex'
                , limitName: 'pageSize'
            }
            , cols: [[
                { field: 'EmployeeId', width: 80, title: '编号' }
                , { field: 'EmployeeName', width: 150, title: '姓名' }
                , { field: 'EmployeeAge', title: '年龄', width: 100 }
                , { field: 'Education', width: 180, title: '学历', templet: '#EducationTmp' }
                , { field: 'ContactPhone', width: 150, title: '联系方式' }
            ]]
        });

        //筛选
        $('#btn_CellStyleSearch').on('click', function () {
            //执行重载
            table.reload('cn_GridCellStyleTable', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                , where: {
                    name: $('#txt_BasicIndex_Name').val()
                }
            });
        });

    });