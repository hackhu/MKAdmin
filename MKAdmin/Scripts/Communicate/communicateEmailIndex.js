﻿//表格管理 - 基本表格
layui.use(['jquery', 'ajaxUtil', 'layedit', 'dialog'], function () {
    var $ = layui.jquery
        , ajaxutil = layui.ajaxUtil
        , layedit = layui.layedit
        , dialog = layui.dialog;

    //构建一个默认的编辑器
    var index = layedit.build('txt_Communicate_Body', {
        tool: ['strong' //加粗
            , 'italic' //斜体
            , 'underline' //下划线
            , 'del' //删除线
            , '|' //分割线
            , 'left' //左对齐
            , 'center' //居中对齐
            , 'right' //右对齐
            , 'link' //超链接
            , 'face' //表情
        ]
        , height: 260
    });

    var init = function () {
        initBtnEvent();
    }

    var initBtnEvent = function () {
        //发送
        $('#btn_Communicate_SendEmail').unbind("click");
        $('#btn_Communicate_SendEmail').bind("click", function (e) {
            var to = $("#txt_Communicate_To").val();
            var subject = $("#txt_Communicate_Subject").val();
            var body = layedit.getContent(index);

            var toList = [];
            toList.push(to);
            ajaxutil.request({
                url: '/communicate/email/sendemail'
                , data: {
                    To: toList,
                    Subject: subject,
                    Body: body
                }
                , success: function (res) {
                    if (res.status) {
                        dialog.alert.success(res.msg);
                        $("#btn_Communicate_ResetEmail").click();
                    }
                    else {
                        dialog.alert.fail(res.msg);
                    }
                }
            });
        });

        $("#btn_Communicate_ResetEmail").unbind('click');
        $("#btn_Communicate_ResetEmail").click(function () {
            //重置
            $("#txt_Communicate_To").val('');
            $("#txt_Communicate_Subject").val('');
            //debugger;
            try {
                layedit.setContent(index, '');
            } catch (e) {
                console.log('setContent fail')
            }
        });
    }

    init();
});