﻿//导入Excel文件
layui.use(['form', 'jquery', 'dialog', 'table', 'laypage', 'ajaxUtil', 'uploadUtil', 'permissionUtil'], function () {
    var table = layui.table
        , laypage = layui.laypage
        , $ = layui.jquery
        , dialog = layui.dialog
        , ajaxutil = layui.ajaxUtil
        , form = layui.form
        , uploadUtil = layui.uploadUtil
        , permissionUtil = layui.permissionUtil;
    //permissionUtil.setting(100000);

    //上传控件初始化
    uploadUtil.render({
        elemId: 'btn_ExcelImport_Data'
        //填写需要上传的文件后缀名
        , exts: 'xls|xlsx'
        , data: {
            uploadfile_ant: 'Excel'
        }
        , done: function (res) {
            ajaxutil.request({
                url: '/excel/import/importdata'
                , type: 'Post'
                , data: {
                    catalog: res.data.catalog
                    , fileName: res.data.fileName
                },
                success: function (res) {
                    if (res.status) {
                        dialog.alert.success(res.msg);
                        //刷新表格
                        loadData({
                            name: ""
                        }, 1)
                    }
                    else {
                        dialog.alert.fail(res.msg);
                    }
                }
            });
        }
    });

    //表格渲染
    table.render({
        id: 'cn_GridExcelImport'
        , elem: '#grid_ExcelImport'
        , method: 'post'
        , url: '/table/basictable/list'
        //启动分页
        , page: true
        , height: 'full-100'
        , loading: true
        , text: { none: '未查询到数据' }
        , limits: [20, 50, 100]
        //禁用前端自动排序
        , autoSort: false
        , where: {
            name: ''
        }
        , request: {
            pageName: 'pageIndex'
            , limitName: 'pageSize'
        }
        , cols: [[
            { field: 'EmployeeId', width: 80, title: '编号' }
            , { field: 'EmployeeName', width: 150, title: '姓名' }
            , { field: 'EmployeeAge', title: '年龄', width: 100 }
            , { field: 'Education', width: 180, title: '学历' }
            , { field: 'ContactPhone', width: 150, title: '联系方式' }
        ]]
    });

    var loadData = function (param, pageIndex) {
        //执行重载
        table.reload('cn_GridExcelImport', {
            page: {
                curr: pageIndex //刷新指定页
            }
            , where: param
        });
    }

    //筛选
    $('#btn_ExcelImport_Search').on('click', function () {
        loadData({
            name: $('#txt_ExcelImport_Name').val()
        }, 1)
    });
});