# MKAdmin

#### 介绍
基于LayUI前端框架实现的后端管理系统

#### 项目架构

前端

1.jquery

2.layui

3.echarts

后端

1.Asp.net MVC

2.Injection依赖注入

3.ADO.NET操作数据库

4.SignalR

数据库

SqlServer2012及以上


#### 数据库安装

1. 按顺序执行dataBase里面三个脚本
2. 修改web.config里面连接数据库连接字符串

#### 项目演示地址

 **http://www.mkadmin.cn/** 
