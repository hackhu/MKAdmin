﻿using Aspose.Cells;
using Aspose.Slides.Pptx;
using Aspose.Words;
using MKAdmin.DTO.Web.Common;
using MKAdmin.DTO.Web.File;
using MKAdmin.ToolKit.FileKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MKAdmin.ToolKit
{
    /// <summary>
    /// 文件在线预览
    /// </summary>
    public class OnlinePreviewHelper
    {
        /// <summary>
        /// world,ppt,excel生成html
        /// </summary>
        /// <param name="sourceDocPath">需要转换的文档绝对文件路径</param>
        /// <param name="relativeDocPath">需要转换的文档相对文件路径</param>
        /// <returns></returns>
        public static Result<OnlinePreviewModel> OfficeDocumentToHtml(string sourceDocPath, string relativeDocPath)
        {
            var result = new Result<OnlinePreviewModel>()
            {
                data = new OnlinePreviewModel(),
                status = true
            };
            var fileName = Guid.NewGuid().ToString("N").ToLower();
            //var catalogName = $"/Files/Preview/temp/{fileName}";
            var catalogConfig = FileCatalogHelper.Catalog["Preview"];

            var catalogName = $@"/{DateTime.Now.ToString("yyyyMM")}/{DateTime.Now.ToString("yyyyMMdd")}/{fileName}";

            string filePath = $@"{catalogConfig.TempFile.AbsolutePath}{catalogName.Replace("/", "\\")}";
            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);

            result.data.PreviewFileUrl = $"{catalogConfig.TempFile.RelativePath}{catalogName}/{fileName}.html";

            //string saveDocPath = HttpContext.Current.Server.MapPath(result.data.PreviewFileUrl);
            string saveDocPath = $@"{filePath}/{fileName}.html";
            if (!File.Exists(saveDocPath))
            {
                File.WriteAllText(saveDocPath, "", Encoding.UTF8);
            }

            //获取文件扩展名
            string docExtendName = Path.GetExtension(sourceDocPath).ToLower();
            switch (docExtendName)
            {
                case ".txt":
                    TxtToHtml(sourceDocPath, saveDocPath);
                    break;
                case ".doc":
                case ".docx":
                    Document doc = new Document(sourceDocPath);
                    doc.Save(saveDocPath, Aspose.Words.SaveFormat.Html);
                    break;
                case ".xls":
                case ".xlsx":
                    Workbook workbook = new Workbook(sourceDocPath);
                    workbook.Save(saveDocPath, Aspose.Cells.SaveFormat.Html);
                    break;
                case ".ppt":
                case ".pptx":
                    PresentationEx pres = new PresentationEx(sourceDocPath);
                    pres.Save(saveDocPath, Aspose.Slides.Export.SaveFormat.Html);
                    break;
                case ".pdf":
                    PdfToHtml(relativeDocPath, saveDocPath);
                    break;
                default:
                    result.status = false;
                    //删除生成的预览文件
                    //File.Delete(saveDocPath);
                    break;
            }

            return result;
        }

        /// <summary>
        /// pdf生成html
        /// </summary>
        /// <param name="fileName">需要生成的原始文档文件</param>
        /// <param name="saveDocPath"></param>
        /// <returns></returns>
        private static bool PdfToHtml(string fileName, string saveDocPath)
        {
            //模板文件
            string templateFile = HttpContext.Current.Server.MapPath("/Files/Preview/templatepdf.html");
            //---------------------读html模板页面到stringbuilder对象里---- 
            StringBuilder htmltext = new StringBuilder();
            using (StreamReader sr = new StreamReader(templateFile)) //模板页路径
            {
                String line;
                while ((line = sr.ReadLine()) != null)
                {
                    htmltext.Append(line);
                }
                sr.Close();
            }

            fileName = fileName.Replace("\\", "/");
            //----------替换htm里的标记为你想加的内容 
            htmltext.Replace("$PDFFILEPATH", fileName);

            //----------生成htm文件------------------―― 
            using (StreamWriter sw = new StreamWriter(saveDocPath, false,
                Encoding.GetEncoding("utf-8"))) //保存地址
            {
                sw.WriteLine(htmltext);
                sw.Flush();
                sw.Close();
            }

            return true;
        }

        /// <summary>
        /// txt生成html
        /// </summary>
        /// <param name="fileName">需要生成的原始文件绝对路径</param>
        /// <param name="saveDocPath"></param>
        /// <returns></returns>
        private static bool TxtToHtml(string sourceFileName, string saveDocPath)
        {
            //---------------------读html模板页面到stringbuilder对象里---- 
            StringBuilder htmltext = new StringBuilder();
            using (StreamReader sr = new StreamReader(sourceFileName, Encoding.GetEncoding("gb2312"))) //模板页路径
            {
                String line;
                while ((line = sr.ReadLine()) != null)
                {
                    //为空就为换行
                    if (string.IsNullOrEmpty(line))
                    {
                        line = "<br/>";
                    }
                    htmltext.Append($"<div>{line}</div>");
                }
                sr.Close();
            }

            //----------生成htm文件------------------―― 
            using (StreamWriter sw = new StreamWriter(saveDocPath, false,
                Encoding.GetEncoding("utf-8"))) //保存地址
            {
                sw.WriteLine(htmltext);
                sw.Flush();
                sw.Close();
            }

            return true;
        }
    }
}
