﻿using MKAdmin.DTO.Web.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace MKAdmin.ToolKit
{
    public static class TxtHelper
    {
        /// <summary>
        /// 获取文件内容
        /// </summary>
        /// <param name="fileName">文件名</param>
        /// <returns></returns>
        public static Result<string> GetFileContent(string fileName)
        {
            var result = new Result<string>();

            string dirPath = HttpContext.Current.Server.MapPath(fileName);

            StreamReader sr = new StreamReader(dirPath, Encoding.Default);
            result.data = sr.ReadToEnd();
            sr.Close();

            return result;
        }
    }
}