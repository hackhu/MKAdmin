﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.ToolKit
{
    public static class CommonUtilHelper
    {
        /// <summary>
        /// 站点启动路径 绝对路径
        /// </summary>
        public static readonly string ROOT_PATH = AppDomain.CurrentDomain.BaseDirectory;

        /// <summary>
        /// 获取GUID
        /// </summary>
        public static string GUID
        {
            get
            {
                return Guid.NewGuid().ToString("N");
            }
        }


        /// <summary>
        /// 获取哈希散列值
        /// </summary>
        /// <param name="source">字符串（需被加密的字符串）</param>
        /// <returns>加密后字符串</returns>
        public static string Encryption(string source)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.UTF8.GetBytes(source));
            StringBuilder sBuilder = new StringBuilder(32);
            for (int i = 0; i < data.Length; i++)
                sBuilder.Append(data[i].ToString("x2"));

            return sBuilder.ToString().ToUpper().Substring(3, 23);
        }

        /// <summary>
        /// 密钥
        /// </summary>
        const string KEY = "USeeUSEE";
        /// <summary>
        /// 向量
        /// </summary>
        const string IV = "20150915";

        /// <summary>
        /// 对称加密
        /// </summary>
        /// <param name="content">内容</param>
        public static string SymmEncryption(string content)
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByteArray = Encoding.Default.GetBytes(content);
            des.Key = ASCIIEncoding.ASCII.GetBytes(KEY);
            des.IV = ASCIIEncoding.ASCII.GetBytes(IV);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            StringBuilder ret = new StringBuilder();
            foreach (byte b in ms.ToArray())
            {
                ret.AppendFormat("{0:X2}", b);
            }
            ret.ToString();
            return ret.ToString();
        }

        /// <summary>
        /// 对称解密
        /// </summary>
        /// <param name="content">内容</param>
        /// <returns>解密后内容</returns>
        public static string SymmDecryption(string content)
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByteArray = new byte[content.Length / 2];
            for (int x = 0; x < content.Length / 2; x++)
            {
                int i = (Convert.ToInt32(content.Substring(x * 2, 2), 16));
                inputByteArray[x] = (byte)i;
            }
            des.Key = ASCIIEncoding.ASCII.GetBytes(KEY);
            des.IV = ASCIIEncoding.ASCII.GetBytes(IV);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            StringBuilder ret = new StringBuilder();
            return System.Text.Encoding.Default.GetString(ms.ToArray());
        }
    }
}
