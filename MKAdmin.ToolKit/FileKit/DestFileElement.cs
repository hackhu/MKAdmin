﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.ToolKit.FileKit
{
    public class DestFileElement : ConfigurationElement
    {
        /// <summary>
        /// 绝对路径
        /// </summary>
        [ConfigurationProperty("AbsolutePath")]
        public string AbsolutePath
        {
            get
            {
                return (string)this["AbsolutePath"];
            }
            set
            {
                base["AbsolutePath"] = value;
            }
        }

        /// <summary>
        /// 相对路径
        /// </summary>
        [ConfigurationProperty("RelativePath")]
        public string RelativePath
        {
            get
            {
                return (string)this["RelativePath"];
            }
            set
            {
                base["RelativePath"] = value;
            }
        }
    }
}
