﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.ToolKit
{
    public static class SecurityHelper
    {
        #region 系统密码加密
        /// <summary>
        /// 系统密码加密
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Encrypt(this string value)
        {
            return Md5Encrypt(Md5Encrypt(value));
        }
        #endregion

        #region MD5

        /// <summary>
        /// MD5加密
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Md5Encrypt(this string value)
        {
            return Md5Encrypt(value, Encoding.UTF8);
        }

        public static string Md5Encrypt(this string value, Encoding encoding)
        {
            using (var md5 = MD5.Create())
            {
                var data = md5.ComputeHash(encoding.GetBytes(value));
                var sBuilder = new StringBuilder();
                foreach (var t in data)
                {
                    sBuilder.Append(t.ToString("x2"));
                }
                return sBuilder.ToString().ToUpper();
            }
        }
        #endregion
    }
}
