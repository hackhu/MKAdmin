﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CachingFramework.Redis;

namespace MKAdmin.ToolKit.CacheHelper
{
    public class RedisHelper
    {
        /// <summary>
        /// redis连接字符串
        /// </summary>
        private static readonly string RedisConnectionString = AppSetting.RedisConnection;

        /// <summary>
        /// RedisCache上下文对象延迟
        /// </summary>
        private static readonly Lazy<RedisContext> LazyContext = new Lazy<RedisContext>(() => new RedisContext(RedisConnectionString));

        /// <summary>
        /// RedisCache上下文对象
        /// </summary>
        private static RedisContext Context = LazyContext.Value;

        /// <summary>
        /// Redis连接状态
        /// </summary>
        /// <returns>true：成功</returns>
        private static bool CanConnected()
        {
            try
            {
                return Context.GetConnectionMultiplexer().IsConnected;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Redis 连接异常：" + ex.ToString());
            }

            return false;
        }

        #region Object

        /// <summary>
        /// 设置redis
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expiry"></param>
        /// <returns></returns>
        public static bool Set(string key, object value, TimeSpan? expiry = null)
        {
            if (!CanConnected() || string.IsNullOrWhiteSpace(key))
                return false;

            try
            {
                Context.Cache.SetObject(key, value, expiry); //value.GetType().IsValueType
                return true;
            }
            catch (Exception ex)
            {
                // 异常日志
                LogHelper.Error("Redis Set异常：" + ex.ToString());
            }

            return false;
        }

        /// <summary>
        /// 获取redis
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Get<T>(string key)
        {
            bool flag;
            return Get<T>(key, out flag);
        }

        /// <summary>
        /// 获取redis
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="flag"></param>
        /// <returns></returns>
        public static T Get<T>(string key, out bool flag)
        {
            if (CanConnected() && !string.IsNullOrWhiteSpace(key))
            {
                try
                {
                    var result = Context.Cache.GetObject<T>(key);
                    flag = null != result;
                    return result;
                }
                catch (Exception ex)
                {
                    // 异常日志
                    LogHelper.Error("Redis Get异常：" + ex.ToString());
                }
            }

            flag = false;
            return default(T);
        }

        #endregion

        #region Check

        /// <summary>
        /// redis key是否存在
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool KeyExist(string key)
        {
            if (!CanConnected()) return false;

            try
            {
                return Context.Cache.KeyExists(key);
            }
            catch (Exception ex)
            {
                // 异常日志
                LogHelper.Error("Redis KeyExist异常：" + ex.ToString());
            }

            return false;
        }

        #endregion

        #region Remove

        /// <summary>
        /// 移除redis
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool Remove(string key)
        {
            if (!CanConnected()) return false;

            try
            {
                return Context.Cache.Remove(key);
            }
            catch (Exception ex)
            {
                // 异常日志
                LogHelper.Error("Redis Remove异常：" + ex.ToString());
            }

            return false;
        }

        /// <summary>
        /// 移除redis
        /// </summary>
        /// <param name="keys"></param>
        public static void Remove(string[] keys)
        {
            if (!CanConnected()) return;
            if (null == keys || keys.Length == 0) return;

            try
            {
                Context.Cache.Remove(keys);
            }
            catch (Exception ex)
            {
                // 异常日志
                LogHelper.Error("Redis Remove异常：" + ex.ToString());
            }
        }

        #endregion
    }
}
